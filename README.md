## Web form Plugin

The web form plugin is an intutable plugin that is used to connect the external [onlineformular-app](https://gitlab.com/aj.bhatia/onlineformular-app) to the [dekanant-app](https://gitlab.com/pvs-hd-teaching/2022-ws/dekanat-app/-/tree/feat/f1000/web-forms). Through this plugin, we are able to send and recieve data from the dekanat-app that is used within the onlineformular-app. The main functionalities of the plugin are as follows:

- Retrieve and insert data into databases within the plugin or the dekanat-app
- Send emails to users of the onlineformular-app
- Find and load the correct forms that are rendered on the onlineformular-app

### Configuration
A majority of the application will work from the get-go, but for specific functionality configuration is required. To be able to send emails, an **email**, **password**, **user**, and **host** need to be set. Once set, this is the email server that will send out the emails. As of now, this is configured to be sent from the Universität Heidelberg mail server, so only university emails can be used with this server. 
All of these fields can be set within `src/auth/emailCredentials.ts` in the format:

- host: "mail.urz.uni-heidelberg.de"
- user: "ab123"
- email: "ab123@stud.uni-heidelberg.de"
- pass: "password"

###### **Note**
Be sure not to push any confidential credentials into the git history!

### Usage with the dekanat-app

To use the web form plugin with the dekanat-app, you can reference it from the `package.json` file as follows (assuming that the plugin is stored in the parent directory):

```javascript
"dependencies": {
    "@intutable/web-forms": "file:../web-forms",
}
```

Once the plugin is added to the registry, the relative reference won't be necessary anymore.

Remember to run `$ npm install`.

## Troubleshooting
If you have any question or the plugin does not work as expected, please contact @aj.bhatia or @j.hildenbrand\_
