import { submitRegistration, requestWebForm, submitForm } from "../src/requests"
import path from "path"
import { Core, EventSystem } from "@intutable/core"
import {
    closeConnection,
    createTable,
    deleteTable,
    insert,
    openConnection,
    select,
} from "@intutable/database/dist/requests"
import { PASSWORD, USERNAME } from "../src/config/connection"
import { allTestTables, testTableNames } from "./exmapleData"
import { allTables, tableNames } from "../src/database/schema"
import {
    exampleFormFilledOut,
    phdAdmissionTableData,
    testTableData,
    requestedForm1,
    requestedForm2,
    requestedForm3,
} from "./exmapleData"

let core: Core
let connectionId: string
const formID = "phdAdmissions"

beforeAll(async () => {
    core = await Core.create(
        [path.join(__dirname, "../node_modules/@intutable/*"), path.join(__dirname, "..")],
        new EventSystem(false)
    )
    connectionId = await core.events
        .request(openConnection(PASSWORD, USERNAME))
        .then(({ connectionId }) => connectionId)
})

afterAll(async () => {
    await core.events.request(closeConnection(connectionId))
    core.plugins.closeAll()
})

//****************************************************************************************************** */

beforeEach(async () => {
    await core.events.request(
        createTable(
            connectionId,
            tableNames.phdadmission,
            allTables[tableNames.phdadmission].schema
        )
    )
    await core.events.request(
        createTable(
            connectionId,
            testTableNames.testTable,
            allTestTables[testTableNames.testTable].schema
        )
    )
    await core.events.request(insert(connectionId, tableNames.phdadmission, phdAdmissionTableData))
    await core.events.request(insert(connectionId, testTableNames.testTable, testTableData))
})

afterEach(async () => {
    await core.events.request(deleteTable(connectionId, tableNames.phdadmission))
    await core.events.request(deleteTable(connectionId, testTableNames.testTable))
})

//****************************************************************************************************** */

describe("Test insertRegistration ", () => {
    test("Applicant already exists in application Process", async () => {
        //Arrange
        const email: string = "example@Mail.de"

        await core.events.request(insert(connectionId, tableNames.phdadmission, email))
        //Act
        const response = await core.events.request(submitRegistration(email, formID))
        //Assert
        expect(response).toEqual(
            expect.objectContaining({
                message: "User is already registered",
            })
        )
    })
    test("Applicant registers for the first time", async () => {
        //Arrange
        const email: string = "example@Mail.de"
        //Act
        const phdTableData = await core.events.request(
            select(connectionId, tableNames.phdadmission)
        )
        let expectedMail = ""
        await core.events.request(submitRegistration(email, formID))
        for (let i = 0; i < phdTableData.length; i++) {
            if (phdTableData[i].email == email) {
                expectedMail = phdTableData[i].email
            }
        }
        //Assert
        expect(expectedMail).toStrictEqual(email)
    }),
        test("Check registration for application linked to multiple forms", async () => {
            //Arrange
            const email: string = "example@Mail.de"
            await core.events.request(submitRegistration(email, "testApplication"))
            //Act
            let someDataInPhd = ""
            let someDataInTest = ""
            const phdTableData = await core.events.request(
                select(connectionId, tableNames.phdadmission)
            )
            const testTableData = await core.events.request(
                select(connectionId, testTableNames.testTable)
            )
            //for both tables, look up if some data was inserted
            for (let i = 0; i < phdTableData.length; i++) {
                if (phdTableData[i].email == email) {
                    someDataInPhd = phdTableData[i].email
                }
            }
            for (let i = 0; i < testTableData.length; i++) {
                if (testTableData[i].email == email) {
                    someDataInTest = testTableData[i].email
                }
            }
            //Assert, make sure data is inserted in both tables
            expect(email + email).toStrictEqual(someDataInPhd + someDataInTest)
        })
})

describe("Test request Webform PhdAdmission", () => {
    test("Applicant is registered and verified and requests initial Form", async () => {
        //Arrange
        const webForm = requestedForm1
        //Act
        const response = await core.events.request(requestWebForm("0", formID))
        //Assert
        expect(response).toStrictEqual({
            message: "Web form loaded successfully",
            webForm,
        })
    })

    test("Applicant filled out initial form and waits for his next turn in the process", async () => {
        //Arrange
        const webForm = requestedForm2
        //Act
        const response = await core.events.request(requestWebForm("1", formID))
        //Assert
        expect(response).toStrictEqual({
            message: "Web form loaded successfully",
            webForm,
        })
    })
    test("Professor saw application and decided for 3 addtional Requirements", async () => {
        //Arrange
        const webForm = requestedForm3
        //Act
        const response = await core.events.request(requestWebForm("4", formID))
        //Assert
        expect(response).toStrictEqual({
            message: "Web form loaded successfully",
            webForm,
        })
    })
    test("Applicant is registered but not verified and requests initial Form", async () => {
        //Arrange
        const Id = phdAdmissionTableData[8]._id
        //Act
        const response = await core.events.request(requestWebForm(Id, formID))
        //Assert
        expect(response).toStrictEqual({
            message: "Applicant is not yet verified",
        })
    })
})

describe("Test submitting webform/uploadingData", () => {
    test("Applicant submitts first and lastname", async () => {
        //Arrange
        const webForm = exampleFormFilledOut
        await core.events.request(submitForm(webForm))
        const tableData = await core.events.request(select(connectionId, tableNames.phdadmission))
        let targetRowIndex: number = 0
        for (let i = 0; i < tableData.length; i++) {
            if (tableData[i]._id == webForm._id) {
                targetRowIndex = i
            }
        }
        //Act
        const response = {
            firstname: tableData[targetRowIndex].firstname,
            lastname: tableData[targetRowIndex].lastname,
        }
        //Assert
        expect(response).toStrictEqual({
            firstname: webForm.form[1].input,
            lastname: webForm.form[0].input,
        })
    })
})
