import { formData } from "../src/types/types"
import { tableNames } from "../src/database/schema"
import { Column, ColumnType } from "@intutable/database/dist/types"

const tableNamePrefix = "webForm_"
export const testTableNames = {
    phdadmission: tableNamePrefix + "phdAdmission",
    testTable: tableNamePrefix + "testTable",
}

const TESTTABLE: Column[] = [
    {
        name: "_id",
        type: ColumnType.string,
    },
    {
        name: "email",
        type: ColumnType.string,
    },
    {
        name: "creationDate",
        type: ColumnType.string,
    },
]

export const allTestTables: Record<string, { schema: Column[]; data?: unknown[] }> = {
    [testTableNames.testTable]: {
        schema: TESTTABLE,
    },
}

export const testTableData = [
    {
        _id: "Ja",
        email: "Ja",
        creationDate: "Ja",
    },
    {
        _id: "Nein",
        email: "Nein",
        creationDate: "Ja",
    },
]

export const phdAdmissionTableData = [
    //Applicant is registered and verified and requests initial Form
    // -> requestedForm1
    {
        index: 0,
        _id: "0",
        email: "john",
        creationDate: "26.",
        verified: true,
    },
    {
        //Applicant filled out initial form and waits for his next turn in the process
        // -> requestedForm2
        index: 1,
        _id: "1",
        email: "ceren",
        creationDate: "1",
        lastname: "topcubasi",
        firstname: "ceren",
        interdisciplinary: "Ja",
        resume: "pathToMyHeibox",
        verified: true,
    },
    {
        //DOE decided that form has to be re-submitted
        // -> requestedForm3
        index: 2,
        _id: "2",
        email: "ceren",
        creationDate: "1",
        lastname: "topcubasi",
        firstname: "ceren",
        interdisciplinary: "Ja",
        resume: "pathToMyHeibox",
        documentsComplete: "N: schon angefragt",
        verified: true,
    },
    {
        //Applicant re-submitted data, DOE approved, processed it and waits for prof to decide for add. Requ.
        // -> requestedForm2
        index: 3,
        _id: "3",
        email: "ajay@",
        creationDate: "1",
        lastname: "ajay",
        firstname: "bhatia",
        interdisciplinary: "Ja",
        resume: "pathToMyHeibox",
        supervisorfacultymember: "coopted",
        casetype: "new",
        documentsComplete: "Ja, in Heibox",
        verified: true,
    },
    {
        //Prof decided for 3 additional Requirements
        // -> requestedForm4
        index: 4,
        _id: "4",
        email: "ajay@",
        creationDate: "1",
        lastname: "ajay",
        firstname: "bhatia",
        interdisciplinary: "Ja",
        resume: "pathToMyHeibox",
        supervisorfacultymember: "coopted",
        casetype: "new",
        documentsComplete: "Ja, in Heibox",
        statusTodo: "Neinthing",
        requirements1: "Ja",
        requirements2: "Ja",
        requirements3: "Ja",
        verified: true,
    },
    {
        //Prof checked additional Requirments and approved, application is successfull
        // -> requestedForm5
        index: 5,
        _id: "5",
        email: "ajay@",
        creationDate: "1",
        lastname: "ajay",
        firstname: "bhatia",
        interdisciplinary: "Ja",
        resume: "pathToMyHeibox",
        supervisorfacultymember: "coopted",
        casetype: "new",
        documentsComplete: "Ja, in Heibox",
        statusTodo: "Neinthing",
        decision: "Zugel. mit 3 Aufl.",
        requirements1: "Ja",
        requirements2: "Ja",
        requirements3: "Ja",
        verified: true,
    },
    {
        //Prof denied the application
        // -> requestedForm6
        index: 6,
        _id: "6",
        email: "ajay@",
        creationDate: "1",
        lastname: "ajay",
        firstname: "bhatia",
        interdisciplinary: "Ja",
        resume: "pathToMyHeibox",
        supervisorfacultymember: "coopted",
        casetype: "new",
        documentsComplete: "Ja, in Heibox",
        statusTodo: "Neinthing",
        decision: "Änd. abgelehnt",
        requirements1: "Ja",
        requirements2: "Ja",
        requirements3: "Ja",
        verified: true,
    },
    //registered but not verified
    //
    {
        index: 7,
        _id: "0",
        email: "john",
        creationDate: "26.",
        verified: false,
    },
]

export const exampleFormFilledOut: formData = {
    _id: "0",
    form: [
        {
            tableId: tableNames.phdadmission,
            input: "hildenbrand",
            columnId: "lastname",
        },
        {
            tableId: tableNames.phdadmission,
            input: "john",
            columnId: "firstname",
        },
    ],
}

export const requestedForm1 = {
    formTitle: "Promotionsannahme",
    formDescription: "Fill in application data",
    fields: [
        {
            fieldText: "Name",
            fieldType: "Input",
            fieldDescription: "Geben Sie Ihren Namen ein",
            isRequired: true,
            renderValue: true,
            table: "webForm_phdAdmission",
            column: "lastname",
        },
        {
            fieldText: "Vorname",
            fieldType: "Input",
            fieldDescription: "Geben Sie Ihren Vornamen ein",
            isRequired: true,
            renderValue: true,
            table: "webForm_phdAdmission",
            column: "firstname",
        },
        {
            fieldText: "Interdisciplinary",
            fieldType: "Dropdown",
            fieldDescription: "Interdisziplinär?",
            isRequired: false,
            table: "webForm_phdAdmission",
            column: "interdisciplinary",
            values: ["Ja", "Nein"],
        },
        {
            fieldText: "Wichtige Unterlagen",
            fieldType: "File",
            fieldDescription: "Geben Sie Ihre Bewerbungsunterlagen ab",
            fileType: "application/pdf",
            repositoryName: "PhDAdmissions",
            userEmail: "uni-id@uni-heidelberg.de",
            userPassword: "*******",
            webdavPassword: "*******",
            directoryPrefix: "/",
            isRequired: false,
            table: "webForm_phdAdmission",
            column: "resume",
        },
    ],
}

export const requestedForm2 = {
    formTitle: "Promotionsannahme",
    formDescription: "Ihr Bewerbungstatus...",
    fields: [
        {
            fieldText: "Ihre Bewerbung ist in Bearbeitung",
            fieldType: "Text",
            table: "",
            column: "",
        },
    ],
}

export const requestedForm3 = {
    formTitle: "Promotionsannahme",
    formDescription: "Bitte geben Sie die fehlenden Informationen erneut ab",
    fields: [
        {
            fieldText: "Name",
            fieldType: "Input",
            fieldDescription: "Geben Sie Ihren Namen ein",
            isRequired: true,
            renderValue: true,
            table: "webForm_phdAdmission",
            column: "lastname",
            value: "topcubasi",
        },
        {
            fieldText: "Vorname",
            fieldType: "Input",
            fieldDescription: "Geben Sie Ihren Vornamen ein",
            isRequired: true,
            renderValue: true,
            table: "webForm_phdAdmission",
            column: "firstname",
            value: "ceren",
        },
        {
            fieldText: "Interdisziplinär",
            fieldType: "Dropdown",
            fieldDescription: "Interdisziplinär?",
            isRequired: false,
            renderValue: true,
            table: "webForm_phdAdmission",
            column: "interdisciplinary",
            values: ["Ja", "Nein"],
            value: "Ja",
        },
        {
            fieldText: "Wichtige Unterlagen",
            fieldType: "File",
            fieldDescription: "Geben Sie Ihre Bewerbungsunterlagen ab",
            fileType: "application/pdf",
            repositoryName: "PhDAdmissions",
            userEmail: "uni-id@uni-heidelberg.de",
            userPassword: "*******",
            webdavPassword: "*******",
            directoryPrefix: "/",
            isRequired: false,
            table: "webForm_phdAdmission",
            column: "resume",
        },
    ],
}

export const requestedForm4 = {
    formTitle: "Promotionsannahme",
    formDescription: "Geben sie die zusätzlichen Anforderungen an",
    fields: [
        {
            fieldText: "Zusatzanforderung 1",
            fieldType: "File",
            fieldDescription: "",
            fileType: "application/pdf",
            repositoryName: "PhDAdmissions",
            userEmail: "uni-id@uni-heidelberg.de",
            userPassword: "*******",
            webdavPassword: "*******",
            directoryPrefix: "/",
            isRequired: false,
            table: "webForm_phdAdmission",
            column: "requirements1",
        },
        {
            fieldText: "Zusatzanforderung 2",
            fieldType: "File",
            fieldDescription: "",
            fileType: "application/pdf",
            repositoryName: "PhDAdmissions",
            userEmail: "uni-id@uni-heidelberg.de",
            userPassword: "*******",
            webdavPassword: "*******",
            directoryPrefix: "/",
            isRequired: false,
            table: "webForm_phdAdmission",
            column: "requirements2",
        },
        {
            fieldText: "Zusatzanforderung 3",
            fieldType: "File",
            fieldDescription: "",
            fileType: "application/pdf",
            repositoryName: "PhDAdmissions",
            userEmail: "uni-id@uni-heidelberg.de",
            userPassword: "*******",
            webdavPassword: "*******",
            directoryPrefix: "/",
            isRequired: false,
            table: "webForm_phdAdmission",
            column: "requirements3",
        },
    ],
}

export const requestedForm5 = {
    formTitle: "Promotionsannahme",
    formDescription: "Resultate Ihrer Bewerbung...",
    fields: [
        {
            fieldText: "Herzlichen Glückwunsch, Ihre Bewerbung wurde angenommen",
            fieldType: "Text",
            table: "",
            column: "",
        },
    ],
}

export const requestedForm6 = {
    formTitle: "Promotionsannahme",
    formDescription: "Resultate Ihrer Bewerbung...",
    fields: [
        {
            fieldText: "Leider wurde Ihre Bewerbung abgelehnt",
            fieldType: "Text",
            table: "",
            column: "",
        },
    ],
}
