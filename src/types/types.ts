import { Field } from "../webFormData/webFormScheme"

export type registrationData = {
    email: string
    _id: string
    creationDate: string
}

export type Row = {
    readonly _id: number
    index: number
    [key: string]: unknown
}

export type formData = {
    _id: string
    workflowTrigger?: string
    form: singleFormData[]
}

export type singleFormData = {
    tableId: string
    input: string | number
    columnId: string
}

export type EmailData = {
    id: string
    email: string
    subject: string
    templateFile: string
    formId: string
}

export type formRequest = {
    formTitle: string
    formDescription: string
    workflowTrigger?: string
    fields: Field[]
    onlyText: boolean
}

export type FormInformation = {
    formId: string
    formTitle: string
}
