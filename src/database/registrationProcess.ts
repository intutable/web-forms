import { PluginLoader } from "@intutable/core"
import { closeConnection, insert, openConnection, select } from "@intutable/database/dist/requests"
import { USERNAME, PASSWORD } from "../config/connection"
import { registrationData } from "../types/types"

let core: PluginLoader

export function initRegistrationProcess(_core: PluginLoader) {
    core = _core
}

export async function insertRegistration(
    data: registrationData,
    linkedTables: string[]
): Promise<void> {
    const connectionId = (await core.events.request(openConnection(USERNAME, PASSWORD)))
        .connectionId

    for (let i = 0; i < linkedTables.length; i++) {
        await core.events.request(insert(connectionId, linkedTables[i], data))
    }

    await core.events.request(closeConnection(connectionId))
}

//If already registered, email will be present in the db
export async function checkIfAlreadyRegistered(
    email: string,
    linkedTables: string[]
): Promise<string> {
    const connectionId = (await core.events.request(openConnection(USERNAME, PASSWORD)))
        .connectionId

    for (let i = 0; i < linkedTables.length; i++) {
        const tableData = await core.events.request(select(connectionId, linkedTables[i]))

        for (var j = 0; j < tableData.length; j++) {
            if (tableData[j]["email"] == email) {
                return tableData[j]["_id"]
            }
        }
    }
    await core.events.request(closeConnection(connectionId))
    return ""
}
