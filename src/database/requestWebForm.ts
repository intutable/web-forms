import { PluginLoader } from "@intutable/core"
import { closeConnection, openConnection, select } from "@intutable/database/dist/requests"
import { USERNAME, PASSWORD } from "../config/connection"
import { Form } from "../webFormData/webFormScheme"
import { formRequest, Row } from "../types/types"

let core: PluginLoader
let rows: Map<string, Row> = new Map()

export function initRequestWebForm(_core: PluginLoader) {
    core = _core
}

export async function getCurrentRows(id: string, tempForm: Form): Promise<Map<string, Row>> {
    const connectionId = (await core.events.request(openConnection(USERNAME, PASSWORD)))
        .connectionId
   
    for (let table of tempForm.tables) {
        const currentTable = await core.events.request(select(connectionId, table))
        for (let row of currentTable) {
            if (row._id && row._id.toString() == id) {
                rows.set(table, row)
                break
            }
        }
    }
    await core.events.request(closeConnection(connectionId))
    return rows
}

export async function getCurrentForm(
    tempForm: Form,
    rows: Map<string, Row>
): Promise<formRequest | undefined> {
    let onlyText = true
    for (let form of tempForm.forms) {
        let trues: number[] = []
        const currentConditions = form.conditions

        if (currentConditions.length === 0) {
            let requestedForm: formRequest = {
                formTitle: tempForm.formTitle,
                formDescription: form.formDescription,
                fields: form.fields,
                onlyText: onlyText,
            }
            return requestedForm
        }

        let index = 0

        for (let condition of currentConditions) {
            const currentColumn = condition.column
            let row = getCurrentRow(condition.table, rows)
            if (row) {
                type currentRowKey = keyof typeof row
                if (
                    (condition.modifier == "!" &&
                        condition.expectedValue != row[currentColumn as currentRowKey]) ||
                    (condition.expectedValue === "%%%" &&
                        row[currentColumn as currentRowKey] !== "" &&
                        row[currentColumn as currentRowKey] !== null &&
                        row[currentColumn as currentRowKey] !== undefined) ||
                    (condition.expectedValue === "" &&
                        (row[currentColumn as currentRowKey] == "" ||
                            row[currentColumn as currentRowKey] == undefined ||
                            row[currentColumn as currentRowKey] == null)) ||
                    condition.expectedValue === row[currentColumn as currentRowKey]
                ) {
                    trues.push(index)
                }
            }
            index++
        }

        let logicArr: boolean[] = []
        index = 0
        //check if trues has all of  for each bracket we say true but if one element not found then logicArr false
        for (let conditional of form.logic) {
            logicArr.push(true)
            for (let element of conditional) {
                if (!trues.includes(element)) {
                    logicArr[index] = false
                    break
                }
            }
            index++
        }
        if (logicArr.includes(true)) {
            for (let field of form.fields) {
                if (field.fieldType != "Text") {
                    onlyText = false
                }
                if (field.fieldType == "File") {
                    field.fileName = "%%%"
                }
                if (field.renderValue) {
                    const currentColumn = field.column
                    const row = getCurrentRow(field.table, rows)
                    if (row) {
                        type currentRowKey = keyof typeof row

                        field.value = row[currentColumn as currentRowKey]
                            ? (row[currentColumn as currentRowKey] as string)
                            : "%%%"
                    }
                } else {
                    if (field.value == "" || field.value == undefined || field.value == null) {
                        field.value = "%%%"
                    }
                }
            }

            //build form that will be sent back
            let requestedForm: formRequest = {
                formTitle: tempForm.formTitle,
                formDescription: form.formDescription,
                workflowTrigger: form.workflowTrigger,
                fields: form.fields,
                onlyText: onlyText,
            }
            return requestedForm
        }
    }
    return undefined
}

export function getCurrentRow(table: string, rows: Map<string, Row>) {
    if (rows.size > 0 && rows.has(table)) {
        return rows.get(table)
    }
    return null
}
