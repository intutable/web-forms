import { Column, ColumnType } from "@intutable/database/dist/types"

const tableNamePrefix = "webForm_"
export const tableNames = {
    phdadmission: tableNamePrefix + "phdAdmission",
}

const PHDADMISSION: Column[] = [
    {
        name: "index",
        type: ColumnType.integer,
    },
    {
        name: "_id",
        type: ColumnType.string,
    },
    {
        name: "email",
        type: ColumnType.string,
    },
    {
        name: "creationDate",
        type: ColumnType.string,
    },
    {
        name: "lastname",
        type: ColumnType.string,
    },
    {
        name: "firstname",
        type: ColumnType.string,
    },
    {
        name: "interdisciplinary",
        type: ColumnType.string,
    },
    {
        name: "resume",
        type: ColumnType.string,
    },
    {
        name: "supervisorfacultymember",
        type: ColumnType.string,
    },
    {
        name: "casetype",
        type: ColumnType.string,
    },
    {
        name: "documentsComplete",
        type: ColumnType.string,
    },
    {
        name: "statusTodo",
        type: ColumnType.text,
    },
    {
        name: "decision",
        type: ColumnType.string,
    },
    {
        name: "requirements1",
        type: ColumnType.string,
    },
    {
        name: "requirements2",
        type: ColumnType.string,
    },
    {
        name: "requirements3",
        type: ColumnType.string,
    },
    {
        name: "verified",
        type: ColumnType.boolean,
    },
]

const PHDADMISSION_DATA = [
    {
        index: 0,
        _id: "0",
        email: "example@mail.de",
        creationDate: "1.1.1",
        verified: true,
    },
    {
        index: 1,
        _id: "1",
        email: "example@mail.de",
        creationDate: "1.1.1",
        lastname: "exampleFirstname",
        firstname: "exampleLastname",
        interdisciplinary: "yes",
        resume: "pathToMyHeibox",
        verified: true,
    },
    {
        index: 2,
        _id: "2",
        email: "example@mail.de",
        creationDate: "1.1.1",
        lastname: "exampleFirstname",
        firstname: "exampleLastname",
        interdisciplinary: "yes",
        resume: "pathToMyHeibox",
        supervisorfacultymember: "no",
        casetype: "phd",
        documentsComplete: "yes",
        statusTodo: "nothing",
        requirements1: "firrst",
        requirements2: "seccond",
        requirements3: "thirrd",
        verified: true,
    },
]

export const allTables: Record<string, { schema: Column[]; data?: unknown[] }> = {
    [tableNames.phdadmission]: {
        schema: PHDADMISSION,
        data: PHDADMISSION_DATA,
    },
}
