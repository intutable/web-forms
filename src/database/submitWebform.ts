import { PluginLoader } from "@intutable/core"
import { closeConnection, openConnection, update } from "@intutable/database/dist/requests"
// eslint-disable-next-line @typescript-eslint/ban-ts-comment
// @ts-ignore
import { registerAction } from "@intutable/process-manager/dist/requests"
import { USERNAME, PASSWORD } from "../config/connection"
import { formData } from "../types/types"

let core: PluginLoader

export function initSubmitWebform(_core: PluginLoader) {
    core = _core
}

export async function uploadSubmittedForm(submittedData: formData): Promise<formData> {
    const userId = submittedData._id
    const connectionId = (await core.events.request(openConnection(USERNAME, PASSWORD)))
        .connectionId

    for (let i = 0; i < submittedData.form.length; i++) {
        const input = submittedData.form[i].input
        const tableId = submittedData.form[i].tableId
        const columnId = submittedData.form[i].columnId

        await core.events.request(
            update(connectionId, tableId, {
                condition: ["_id", userId],
                update: { [columnId]: input },
            })
        )
    }
    await core.events.request(closeConnection(connectionId))

    console.dir("PUSH WORKFLOW")
    await pushWorkflow(submittedData)

    return submittedData
}

async function pushWorkflow(submittedData: formData) {
    console.dir("WORKFLOW: ", submittedData.workflowTrigger)
    if (submittedData.workflowTrigger != undefined) {
        const result = await core.events.request(
            registerAction(submittedData.workflowTrigger, undefined, [submittedData._id])
        )
        console.dir("RESULT: ", result)
    }
}
