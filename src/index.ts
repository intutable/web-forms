import { CoreRequest, CoreResponse, PluginLoader } from "@intutable/core"
import {
    CHANNEL,
    submitRegistration,
    requestWebForm,
    submitForm,
    sendEmail,
    verifyUser,
    getAllForms,
} from "./requests"
import {
    insertRegistration,
    checkIfAlreadyRegistered,
    initRegistrationProcess,
} from "./database/registrationProcess"
import { getCurrentRows, getCurrentForm, initRequestWebForm } from "./database/requestWebForm"
import { uploadSubmittedForm, initSubmitWebform } from "./database/submitWebform"
import { sendEmailWTemplate, initSendEmailWTemplate } from "./email/sendEmail"
import { verifyIdEmailPair, initVerifyIdEmailPair } from "./email/verifyUser"
import { EmailData, FormInformation, formRequest, registrationData } from "./types/types"
import { Form } from "./webFormData/webFormScheme"
import forms from "./webFormData/webForm.json"
import { initDatabase } from "./database/initDatabase"
import { v4 as uuidv4 } from "uuid"

let plugins: PluginLoader

export async function init(core: PluginLoader) {
    plugins = core
    await initDatabase(plugins)
    initRegistrationProcess(plugins)
    initRequestWebForm(plugins)
    initSubmitWebform(plugins)
    initSendEmailWTemplate(plugins)
    initVerifyIdEmailPair(plugins)
    plugins
        .listenForRequests(CHANNEL)
        .on(submitRegistration.name, submitRegistration_)
        .on(requestWebForm.name, requestWebForm_)
        .on(submitForm.name, submitForm_)
        .on(sendEmail.name, sendEmail_)
        .on(verifyUser.name, verifyUser_)
        .on(getAllForms.name, getAllForms_)
}

async function sendEmail_({
    email,
    formId,
    id,
    templateFile,
    subject,
}: CoreRequest): Promise<CoreResponse> {
    const emailData: EmailData = {
        email: email,
        id: id,
        formId: formId,
        templateFile: templateFile,
        subject: subject,
    }
    if (!sendEmailWTemplate(emailData)) {
        return {
            message: "Email not sent successfully",
        }
    }
    return {
        message: "Email sent successfully",
    }
}

async function submitRegistration_({ email, formId }: CoreRequest): Promise<CoreResponse> {

    const form = (forms as Form[]).find(data => data.formId == formId) as Form
    const linkedTables: string[] = form.tables
    if (linkedTables == undefined) {
        return {
            message: "Form could not be found",
        }
    }

    const response = await checkIfAlreadyRegistered(email, linkedTables)
    if (response != "") {
        const rowsOfApplicant = await getCurrentRows(response, form)
        let alreadyVerified = false
        for (const [key, value] of rowsOfApplicant.entries()) {
            if (value.verified == true) {
                alreadyVerified = true
            }
        }
        return {
            registrationData: {
                email: email,
                _id: response,
            },
            alreadyVerified: alreadyVerified,
            message: "User is already registered",
        }
    }

    const registrationData: registrationData = {
        email: email,
        _id: uuidv4(),
        creationDate: new Date(Date.now()).toLocaleString(),
    }

    await insertRegistration(registrationData, linkedTables)
    return {
        message: "Registration successful",
        registrationData,
    }
}

async function requestWebForm_({ userId, formId }: CoreRequest): Promise<CoreResponse> {
    const form = (forms as Form[]).find(data => data.formId == formId) as Form

    if (form == undefined) {
        return {
            message: "Form could not be found",
        }
    }
    const tempWebForm = {
        formId: form.formTitle,
        formTitle: form.formTitle,
        fields: []
    }

    const rowsOfApplicant = await getCurrentRows(userId, form)
    if (rowsOfApplicant == undefined || rowsOfApplicant.size == 0) {
        return {
            message: "Applicant could not be found",
            webForm: tempWebForm
        }
    }
    console.dir(rowsOfApplicant)
    for (const [key, value] of rowsOfApplicant.entries()) {
        if (value.verified != true && value.verified != 'true') {
            return {
                message: "Applicant is not yet verified",
                webForm: tempWebForm
            }
        }
    }

    const webForm: formRequest | undefined = await getCurrentForm(form, rowsOfApplicant)

    if (webForm == undefined) {
        return {
            message: "Web form could not be found",
        }
    }

    return {
        message: "Web form loaded successfully",
        webForm,
    }
}

async function submitForm_({ submittedData }: CoreRequest): Promise<CoreResponse> {
    const webForm = await uploadSubmittedForm(submittedData)
    return {
        webForm,
        message: "Submitted Successfully",
    }
}

async function verifyUser_({ userId, formId, email }: CoreRequest): Promise<CoreResponse> {
    const form = (forms as Form[]).find(data => data.formId == formId) as Form
    if (form == undefined) {
        return {
            message: "Form could not be found",
        }
    }
    const linkedTables: string[] = form.tables

    const rowsOfApplicant = await getCurrentRows(userId, form)
    let alreadyVerified = false
    for (const [key, value] of rowsOfApplicant.entries()) {
        if (value.verified == true) {
            alreadyVerified = true
        }
        if (value.email == email) {
            await verifyIdEmailPair(userId, linkedTables)
        } else {
            return {
                message: "Verification failed",
            }
        }
    }
    if (alreadyVerified) {
        return {
            message: "User is already verified",
        }
    }

    return {
        message: "Verification successful",
    }
}

async function getAllForms_(): Promise<CoreResponse> {
    const formInformation: FormInformation[] = []
    for (const form of forms as Form[]) {
        formInformation.push({
            formId: form.formId,
            formTitle: form.formTitle,
        })
    }
    return {
        message: "Forms loaded successfully",
        formInformation,
    }
}
