import { CoreRequest } from "@intutable/core"
import { EmailData, formData } from "./types/types"

export const CHANNEL = "web-forms"

export function submitRegistration(
    email: string,
    formId: string
): CoreRequest {
    return {
        channel: CHANNEL,
        method: submitRegistration.name,
        email,
        formId,
    }
}

export function requestWebForm(id: string, formId: string): CoreRequest {
    return {
        channel: CHANNEL,
        method: requestWebForm.name,
        id,
        formId,
    }
}

export function submitForm(submittedData: formData): CoreRequest {
    return {
        channel: CHANNEL,
        method: submitForm.name,
        submittedData,
    }
}

export function sendEmail(emailData: EmailData): CoreRequest {
    return {
        channel: CHANNEL,
        method: sendEmail.name,
        emailData,
    }
}

export function verifyUser(userId: string, formId: string, email: string): CoreRequest {
    return {
        channel: CHANNEL,
        method: verifyUser.name,
        userId,
        formId,
        email,
    }
}

export function getAllForms(): CoreRequest {
    return {
        channel: CHANNEL,
        method: getAllForms.name
    }
}
