export interface Field {
    fieldText: string
    fieldType: string
    table: string
    column: string
    fieldDescription?: string
    isRequired?: boolean
    renderValue?: boolean
    values?: string[]
    fileName?: string
    value?: any
}

export interface Condition {
    table: string
    column: string
    expectedValue: string
    modifier?: string
}
export interface ConditionalForm {
    formDescription: string
    logic: number[][]
    conditions: Condition[]
    workflowTrigger?: string
    fields: Field[]
}

export interface Form {
    formId: string
    formTitle: string
    tables: string[]
    forms: ConditionalForm[]
}

export default Form
