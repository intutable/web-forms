import { PluginLoader } from "@intutable/core"
import { EmailData } from "../types/types"
import { readFileSync } from "fs"
import { compile } from "handlebars"
import { createTransport, SendMailOptions, Transporter } from "nodemailer"
import { join, resolve } from "path"
import credentials from "../auth/emailCredentials"

let core: PluginLoader

export function initSendEmailWTemplate(_core: PluginLoader) {
    core = _core
}

function initTransporter() {
    return createTransport({
        host: credentials.host,
        port: 587,
        auth: {
            user: credentials.user,
            pass: credentials.pass,
        },
    })
}

function initOptions(emailData: EmailData, html: string) {
    return {
        from: credentials.email,
        to: emailData.email,
        subject: emailData.subject,
        html: html,
    }
}

const readHtmlTemplate = (templateFile: string) => {
    const __dirname = resolve()
    const filePath = join(__dirname, "../../web-forms/src/email/emailTemplates/", templateFile)
    const source = readFileSync(filePath, "utf-8").toString()
    const template = compile(source)
    return template
}

function sendEmailWithOption(transporter: Transporter, options: SendMailOptions) {
    transporter.sendMail(options, function (err: any) {
        if (err) {
            console.dir("Error while sending email", err)
            return false
        }
    })
    return true
}

export function sendEmailWTemplate(emailData: EmailData) {
    const templateFile = emailData.templateFile
    const template = readHtmlTemplate(templateFile)
    const replacements = {
        email: emailData.email,
        webForm: emailData.formId,
        id: emailData.id,
    }
    const html = template(replacements)
    const transporter = initTransporter()
    const options = initOptions(emailData, html)
    return sendEmailWithOption(transporter, options)
}
