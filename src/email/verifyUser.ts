import { PluginLoader } from "@intutable/core"
import { closeConnection, openConnection, update } from "@intutable/database/dist/requests"
import { PASSWORD, USERNAME } from "../config/connection"

let core: PluginLoader

export function initVerifyIdEmailPair(_core: PluginLoader) {
    core = _core
}

export async function verifyIdEmailPair(userId: string, linkedTables: string[]): Promise<any> {
    const connectionId = (await core.events.request(openConnection(USERNAME, PASSWORD)))
        .connectionId

    for (let i = 0; i < linkedTables.length; i++) {
        await core.events.request(
            update(connectionId, linkedTables[i], {
                condition: ["_id", userId],
                update: { verified: true },
            })
        )
    }

    await core.events.request(closeConnection(connectionId))

    return true
}

